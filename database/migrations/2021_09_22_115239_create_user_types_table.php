<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_types', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->timestamps();
        });

        DB::table('user_types')->insert(
            array(
                'name' => 'SUPERADMIN'
            )     
        );
        DB::table('user_types')->insert(
            array(
                'name' => 'ADMIN'
            )   
        );
        DB::table('user_types')->insert(
            array(
                'name' => 'USER'
            )
            
        );
        DB::table('user_types')->insert(
            array(
                'name' => 'CUSTOMER'
            ),
            
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_types');
    }
}
