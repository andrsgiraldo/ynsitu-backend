<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;    
use Illuminate\Support\Str;

class CategoriesController extends Controller{

    public function add(Request $request){
        $permissions = app('App\Http\Controllers\AuthController')->permissions( $request->user() );
        if(isset($permissions["CATEGORIES"])){
            $request->validate([
                'name' => 'required|string'
            ]);
    
            $user = Category::create([
                'name' => $request->name
            ]);

            return response()->json($user);
        }else{
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        } 
    }  
    public function index(){
        $categories = Category::orderBy('name', 'asc')->get();
        $categories_array = [];
        foreach($categories as $category){
            $category->path = "/categories/".$category->id."/";
            array_push($categories_array, $category);
        }
        return response()->json($categories_array);
    } 
    public function get( $id ){
        $category = Category::where('id', $id)->first();

        return response()->json($category);
    } 
}