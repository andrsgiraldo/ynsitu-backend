<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;


class CompaniesController extends Controller{

    public function add(Request $request){
        $permissions = app('App\Http\Controllers\AuthController')->permissions( $request->user() );
        if(isset($permissions["COMPANIES"])){
            $request->validate([
                'nit' => 'required|integer|unique:companies',
                'name' => 'required|string',
                'address' => 'string',
                'phone' => 'numeric'
            ]);
    
            $user = Company::create([
                'nit' => $request->nit,
                'name' => $request->name,
                'address' => $request->address,
                'phone' => $request->phone
            ]);

            return response()->json($user);
        }else{
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        } 
    }  
}