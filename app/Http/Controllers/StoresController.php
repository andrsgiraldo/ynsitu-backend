<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Store;


class StoresController extends Controller{

    public function add(Request $request){
        $permissions = app('App\Http\Controllers\AuthController')->permissions( $request->user() );
        if(isset($permissions["STORES"])){
            $request->validate([
                'name' => 'required|string|unique:stores',
                'company_id' => 'required|integer',
                'address' => 'string',
                'phone' => 'numeric'
            ]);
    
            $user = Store::create([
                'company_id' => $request->company_id,
                'name' => $request->name,
                'address' => $request->address,
                'phone' => $request->phone
            ]);

            return response()->json($user);
        }else{
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        } 
    }  
}