<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;


class ProductsController extends Controller{

    public function add(Request $request){
        $permissions = app('App\Http\Controllers\AuthController')->permissions( $request->user() );
        if(isset($permissions["PRODUCTS"])){
            $request->validate([
                'sku' => 'required|string|unique:products',
                'name' => 'required|string',
                'description' => 'required|string',
                'image' => 'required|string',
                'featured' => 'integer',
                'price' => 'required|numeric',
                'product_category' => 'integer'
            ]);
    
            $user = Product::create([
                'sku' => $request->sku,
                'name' => $request->name,
                'description' => $request->description,
                'image' => $request->image,
                'featured' => $request->featured,
                'price' => $request->price,
                'product_category' => $request->product_category,
            ]);

            return response()->json($user);
        }else{
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        } 
    }  
    public function get( $category ){
        if($category == "all"){
            $products = Product::get();

        }else{
            $products = Product::where('product_category', $category)->get();
        }

        return response()->json($products);
    } 
    public function getById( Request $request ){
        $ids = $request->productsIds;
        $products = Product::whereIn('id', $ids)->get();
    

        return response()->json($products);
    } 
}