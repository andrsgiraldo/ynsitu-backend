<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserPermission;
use App\Models\UserType;


class UserController extends Controller{
    public function add(Request $request){
        $permissions = app('App\Http\Controllers\AuthController')->permissions( $request->user() );
        if(isset($permissions[$request->user_type])){
            $user_types = $this->user_types();
            if(!isset($user_types[$request->user_type])){
                return response()->json([
                    'message' => 'No existe tipo de usuario'
                ], 401);
            }
            
            $request->validate([
                'first_name' => 'required|string',
                'last_name' => 'string',
                'email' => 'required|string|email|unique:users',
                'password' => 'required|string'
            ]);
    
            $user = User::create([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'user_type_id' => $user_types[$request->user_type]->id,
                'password' => bcrypt($request->password)
            ]);
            
            return response()->json($user);
        }else{
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        } 
    }  
    public function user_types(){
        return UserType::all()->keyBy('name');
    }       
}