<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// AUTH
Route::group([
    'prefix' => 'auth',
    'namespace' => 'App\Http\Controllers'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signUp');

    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});

Route::group([
    'namespace' => 'App\Http\Controllers'
], function () {
    Route::get('categories/{id}', 'CategoriesController@get');
    
    Route::get('products/{category}', 'ProductsController@get');

    Route::post('products', 'ProductsController@getById');

    Route::get('categories', 'CategoriesController@index');

    Route::post('signup', 'AuthController@signUp');
    Route::group([
      'middleware' => 'auth:api'
    ], function() {

        /*Crear superadmin, admin, user, customer */
        Route::post('user/add', 'UserController@add');

        /*Crear productos */
        Route::post('products/add', 'ProductsController@add');

        /*Crear Compañias */
        Route::post('company/add', 'CompaniesController@add');

         /*Crear Categorias */
         Route::post('category/add', 'CategoriesController@add');
         
         /*Crear Tiendas */
         Route::post('stores/add', 'StoresController@add');
    });
});